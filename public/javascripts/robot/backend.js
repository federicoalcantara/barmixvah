if (backend == null) {
  var backend = {};
  backend.pins = [27, 28, 3, 5, 7, 29, 31, 26, 24, 21,
    19, 23, 32, 33, 8, 10, 36, 11, 12, 35,
    38, 40, 15, 16, 18, 22, 37, 13];
}

exports.initialize = function () {
  if (backend.Gpio == null) {
    backend.Gpio = require('onoff').Gpio;
    backend.pumps = [];
    console.log("\033[31m[MSG] Bar Mixvah Ready\033[91m");
    exports.initPump(0, 5);
    exports.initPump(1, 6);
    exports.initPump(2, 13);
    exports.initPump(3, 19);
    exports.initPump(4, 26);
    exports.initPump(5, 21);
    exports.initPump(6, 20);
    exports.initPump(7, 16);
    exports.initPump(8, 12);
    exports.initPump(9, 10);
    // Add here more pump as needed.
    // exports.initPump(n, gpio);
  }
}

exports.initPump = function (pumpId, gpio) {
  console.log("\033[31m[MSG] Initializing pump " + pumpId + " at GPIO " + gpio + " (pin " + backend.pins[gpio] + ") \033[91m");
  backend.pumps[pumpId] = new backend.Gpio(gpio, 'out');
  exports.stopPump("pump" + pumpId, true);
}

exports.pump = function (ingredients) {
  console.log("Dispensing Drink");
  for (var i in ingredients) {
    (function (i) {
      setTimeout(function () {  // Delay implemented to have a top-biased mix
        exports.wait(ingredients[i].pump, ingredients[i].amount);
      }, ingredients[i].delay);
    })(i);
  }
};

exports.wait = function pumpMilliseconds(pump, ms) {
  exports.startPump(pump);
  setTimeout(function () {
    exports.stopPump(pump);
  }, ms);
}

exports.startPump = function (pump) {
  exports.initialize();
  console.log("\033[32m[PUMP] Starting " + pump + "\033[91m");
  var p = exports.usePump(pump);
  try {
    p.writeSync(0);
    console.log("\033[32m[PUMP] " + pump + " STARTED \033[91m");
  } catch (err) {
    console.log("ERROR starting Pump :" + pump + ". " + err);
  }
}

exports.stopPump = function (pump, noMessage) {
  var p = exports.usePump(pump);
  if (p != null) {
    if (!noMessage) {
      console.log("\033[32m[PUMP] Stopping " + pump + "\033[91m");
    }
    try {
      p.writeSync(1);
      if (!noMessage) {
        console.log("\033[32m[PUMP] " + pump + " STOPPED \033[91m");
      }
    } catch (err) {
      if (!noMessage) {
        console.log("ERROR stopping Pump :" + pump + ". " + err);
      }
    }
  }
}

exports.usePump = function (pump) {
  var pumpId = parseInt(pump.substring(4));
  return backend.pumps[pumpId];
}

process.on('SIGINT', function() {
  for (var i = 0; i < backend.pumps.length; i++) {
    var pumpName = "pump" + i;
    exports.stopPump(pumpName);
  }
  process.exit(0);
});


